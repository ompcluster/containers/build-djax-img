import re
import pytest

from containers.building_blocks import deepwave as _deepwave

# REGEX ------------------------------------------------------------------------

RE_EDITABLE = re.compile(r"pip install -e")
RE_EXTRAS = re.compile(r"\.\[(\w+)(?:,(\w+))*\]")
RE_BRANCH = re.compile(r"--branch main")
RE_CHECKOUT = re.compile(r"git checkout (\w+)")
RE_MKDIR = re.compile(r"mkdir -p (?P<prefix>/(?:\w+/?)*)")

# HELPER FUNCTIONS -------------------------------------------------------------


def deepwave(*args, **kwargs):
    return str(_deepwave(*args, **kwargs))


# TESTS ------------------------------------------------------------------------


@pytest.mark.parametrize("editable", [True, False])
def test_deepwave_editable(editable):
    match = RE_EDITABLE.search(deepwave(editable=editable))
    assert editable == bool(match)


@pytest.mark.parametrize(
    "extras",
    [
        ("one", "two"),
        ("123", "456"),
    ],
)
def test_deepwave_extras(extras):
    assert (match := RE_EXTRAS.search(deepwave(extras=extras)))
    assert match.groups() == extras


def test_deepwave_version_default():
    assert RE_BRANCH.search(deepwave())


@pytest.mark.parametrize(
    "version", ["named_version", "1.0", "11.22.33", "v0.0.0+extra1.extra_2"]
)
def test_deepwave_version(version):
    assert RE_CHECKOUT.search(deepwave(version=version))


@pytest.mark.parametrize(
    "prefix",
    [
        "/",
        "/a",
        "/abc",
        "/one/two/three/four/give",
        "/path_with_underscores",
        "/path_ending_with_slash/",
    ],
)
def test_deepwave_prefix(prefix):
    assert (match := RE_MKDIR.search(deepwave(prefix=prefix)))
    assert match.group("prefix") == prefix
