import re
import pytest

from containers.building_blocks import alpa as _alpa

# REGEX ------------------------------------------------------------------------

RE_EDITABLE = re.compile(r"pip install -e")
RE_VERSION = re.compile(r"alpa==(?P<version>\S+)")
RE_TENSORFLOW = re.compile(r"python3 build/build.py")
RE_RECURSIVE = re.compile(r"git clone (?:.+) --recursive")
RE_MKDIR = re.compile(r"mkdir -p (?P<prefix>/(?:\w+/?)*)")

# HELPER FUNCTIONS -------------------------------------------------------------


def alpa(*args, **kwargs):
    return str(_alpa(*args, **kwargs)).splitlines()


# TESTS ------------------------------------------------------------------------


@pytest.mark.parametrize("editable", [True, False])
def test_alpa_editable(editable):
    lines = alpa(editable=editable)

    # If the installation is editable, there should be at least a single match
    # of `pip install -e`, if the installation is not editable, there should be
    # no matches at all.
    assert editable == any(RE_EDITABLE.search(line) for line in lines)


def test_alpa_version_default():
    lines = alpa()
    assert all(not RE_VERSION.search(line) for line in lines)


@pytest.mark.parametrize(
    "version", ["named_version", "1.0", "11.22.33", "0.0.0+extra1.extra_2"]
)
def test_alpa_version(version):
    lines = alpa(version=version)

    matches = list(RE_VERSION.search(line) for line in lines)
    match = next(match for match in matches if match)

    assert sum(bool(m) for m in matches) == 1
    assert any(matches)
    assert match.group("version") == version


@pytest.mark.parametrize("build_tensorflow", [True, False])
def test_alpa_build_tensorflow(build_tensorflow):
    lines = alpa(from_pip=False, build_tensorflow=build_tensorflow)

    # If we choose to build tensorflow, there should be at least one match of
    # the tensorflow build script invocation. If we choose not to build it,
    # there should be no matches at all.
    assert build_tensorflow == any(RE_TENSORFLOW.search(line) for line in lines)


@pytest.mark.parametrize(
    "prefix",
    [
        "/",
        "/a",
        "/abc",
        "/one/two/three/four/give",
        "/path_with_underscores",
        "/path_ending_with_slash/",
    ],
)
def test_alpa_prefix(prefix):
    lines = alpa(from_pip=False, prefix=prefix)

    matches = list(RE_MKDIR.search(line) for line in lines)
    match = next(match for match in matches if match)

    assert sum(bool(m) for m in matches) == 1
    assert any(matches)
    assert match.group("prefix") == prefix
