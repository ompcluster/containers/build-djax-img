# DJAX Containers

Container recipe generator with [HPCCM][1].

## Installation

Inside the root directory of this repository run the following command to
install. We recommend installing this project inside a conda environment or
virtual environment.

```bash
pip install -e .
```

## Usage

To generate the default recipe, run the following command.

```bash
containers recipe
```

For more information, run:

```bash
containers recipe --help
```

Finally, to create a container image with Apptainer (Singularity) use the
following command:

```bash
singularity build image.sif singularity.def
```

<!-- REFERENCES --------------------------------------------------------------->

[1]: https://github.com/NVIDIA/hpc-container-maker 
[2]: https://developer.nvidia.com/cudnn
[3]: https://developer.nvidia.com/rdp/cudnn-download
