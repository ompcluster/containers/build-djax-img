# Copyright 2023 DJAX Team
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import logging
import subprocess as sp

# LOGGING ----------------------------------------------------------------------

log = logging.getLogger(__name__)

# UTILS ------------------------------------------------------------------------

build_function = {}


def register_build(name):
    """Decorator to register container stages."""

    def register_build_inner(fn):
        global build
        build_function[name] = fn
        return fn

    return register_build_inner


# BUILD FUNCTIONS --------------------------------------------------------------


@register_build("docker")
def build_docker(recipe, output, bind, *args, **kwargs):
    raise NotImplementedError("Building docker images is not supported yet!")


@register_build("singularity")
def build_singularity(recipe, output, bind, *args, **kwargs):
    command = ["sudo", "-E", "singularity", "build"]
    if bind:
        command += ["-B", bind]
    command += [output, recipe]

    log.info(f"Command to build: {' '.join(command)!r}")
    process = sp.run(command)

    if process.returncode != 0:
        raise RuntimeError("Failed to build singularity image.")
