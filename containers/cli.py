# Copyright 2023 DJAX Team
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import re
import sys
import logging

from time import time
from pathlib import Path
from datetime import timedelta

from containers.recipe import StageCreator
from containers.build import build_function
from containers.logging import setup_logging_handlers

import click
import hpccm  # type: ignore

from click.utils import LazyFile
from click import argument, option, version_option, pass_context
from rich import print

# LOGGING ----------------------------------------------------------------------

log = logging.getLogger(__name__)

# CONSTANTS --------------------------------------------------------------------

KNOWN_PROTOCOLS = {"docker", "file", "recipe"}

FILE_EXTENSION = {
    "docker": "dockerfile",
    "singularity": "def",
}

# CLICK BOILERPLATE ------------------------------------------------------------


class AliasedGroup(click.Group):
    def get_command(self, ctx, cmd_name):
        rv = click.Group.get_command(self, ctx, cmd_name)
        if rv is not None:
            return rv
        matches = [x for x in self.list_commands(ctx) if x.startswith(cmd_name)]
        if not matches:
            return None
        elif len(matches) == 1:
            return click.Group.get_command(self, ctx, matches[0])
        ctx.fail(f"Too many matches: {', '.join(sorted(matches))}")

    def resolve_command(self, ctx, args):
        # always return the full command name
        _, cmd, args = super().resolve_command(ctx, args)
        return cmd.name, cmd, args


class CommaSeparatedListParam(click.ParamType):
    name = "list"

    def convert(self, value, param, ctx):
        if isinstance(value, set):
            return value
        if not isinstance(value, str):
            self.fail(f"{value!r} is not a string.", param, ctx)
        return set(value.split(","))


class BaseRecipeParam(click.ParamType):
    name = "base-recipe"

    regex = re.compile(r"^(?P<protocol>\w+)://(?P<name>.+)$")

    def convert(self, value, param, ctx):
        if not isinstance(value, str):
            self.fail(f"{value!r} is not a string.", param, ctx)

        match = (regex := self.regex).search(value)

        if not match:
            self.fail(f"Parameter must match the pattern {regex.pattern}.")

        protocol = match.group("protocol")
        name = match.group("name")

        return protocol, name


# PARAMETER TYPES --------------------------------------------------------------

InputFile = click.File("r")
OutputFile = click.File("w")
FilePath = click.Path(exists=True, file_okay=True, dir_okay=False, resolve_path=True)
DirPath = click.Path(exists=True, file_okay=False, dir_okay=True, resolve_path=True)
ContainerFormat = click.Choice(["docker", "singularity"])
AvailableStages = click.Choice(StageCreator.stages_names())
List = CommaSeparatedListParam()
BaseRecipe = BaseRecipeParam()

# COMMAND-LINE INTERFACE -------------------------------------------------------


@click.group(cls=AliasedGroup)
@version_option(None, "-V", "--version", message="%(prog)s v%(version)s")
@option("-v", "--verbose", count=True)
@option("-q", "--quiet", is_flag=True)
@pass_context
def cli(ctx, verbose, quiet):
    """Container recipe generation tool with HPCCM."""
    ctx.verbosity = verbose
    ctx.quiet = quiet


@cli.command()
def ls():
    """List available recipes."""
    print("Available recipes:")
    for name, fn in StageCreator.stages.items():
        print(f"- [bold]{name}[/bold]")
        print(f"  Description: {fn.docstring}")
        if len(fn.features) > 0:
            print(f"  Optional features:")
            for feat_name, feat_desc in fn.features_dict.items():
                print(f"    {feat_name!r}: {feat_desc}")


@cli.command()
@argument("recipe_name", type=AvailableStages)
@option("-o", "--output", type=OutputFile, default=None)
@option("-f", "--format", type=ContainerFormat, default="singularity")
@option("-F", "--features", type=List, default=set())
@option("-b", "--base", type=BaseRecipe, default=None)
@option("-c", "--cuda", type=str, default="11.8.0")
def recipe(recipe_name, output, format, features, base, cuda):
    """Generate a container recipe file."""

    hpccm.config.set_container_format(format)  # type: ignore

    if base is not None:
        protocol, base_name = base

        if recipe_name == base_name:
            log.critical(f"Cannot build image {recipe_name!r} with itself as base.")
            sys.exit(1)

        if protocol not in KNOWN_PROTOCOLS:
            log.critical(
                f"Protocol {protocol!r} not recognized, must be one of {KNOWN_PROTOCOLS}."
            )
            sys.exit(1)

        if protocol == "recipe" and base_name not in StageCreator.stages:
            log.critical(
                f"Recipe {base_name!r} not recognized, must be one of {StageCreator.stages_names()}."
            )
            sys.exit(1)

        if protocol == "file" and not Path(base_name).exists():
            log.warning(
                f"Using base image from file {base_name!r} that does not exist."
            )

    if len(features) > 0:
        log.info(f"Optional features = {features}")

    stage = StageCreator.create(
        recipe_name,
        base=base,
        features=features,
        format=format,
    )

    if not output:
        ext = FILE_EXTENSION[str(format)]
        output = LazyFile(f"{recipe_name}.{ext}", mode="w").open()

    print(stage, end="\n", file=output)
    log.info(f"Recipe written to {output.name!r}.")


@cli.command()
@argument("recipe", type=FilePath)
@option("-o", "--output", type=OutputFile, default=None)
@option("-B", "--bind", type=str, default=None)
def build(recipe, output, bind):
    """Build a container image from a recipe file."""

    kind = "singularity" if recipe.endswith("def") else "docker"

    if not output:
        output = Path(recipe).with_suffix("").with_suffix(".sif").name
    else:
        output = output.name

    t = time()
    build_function[kind](recipe, output, bind)
    t = timedelta(seconds=time() - t)

    log.info(f"Container image {output!r} built in {t}.")


# BOILERPLATE ------------------------------------------------------------------


def main():
    setup_logging_handlers()
    cli()


if __name__ == "__main__":
    main()
