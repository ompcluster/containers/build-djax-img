# Copyright 2023 DJAX Team
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Alpa building block."""

import hpccm.templates.git

from hpccm.building_blocks.base import bb_base
from hpccm.primitives.comment import comment
from hpccm.primitives.shell import shell

# CONSTANTS --------------------------------------------------------------------


ALPA_REPO_URL = "https://github.com/alpa-projects/alpa.git"

DEFAULT_JAXLIB_VERSION = "0.3.22+cuda112.cudnn810"

COMPILE_TF_COMMAND = r"""python3 build/build.py \
    --enable_cuda \
    --dev_install \
    --bazel_options="--override_repository=org_tensorflow=$(pwd)/../third_party/tensorflow-alpa"
"""

# RECIPE -----------------------------------------------------------------------


class alpa(bb_base, hpccm.templates.git):
    """Alpa building block."""

    def __init__(self, **kwargs):
        super(alpa, self).__init__(**kwargs)

        self._version = kwargs.get("version", "latest")
        self._from_pip = kwargs.get("from_pip", True)
        self._build_tf = kwargs.get("build_tensorflow", False)
        self._prefix = kwargs.get("prefix", "/opt")
        self._editable = kwargs.get("editable", False)
        self._jaxlib = kwargs.get("jaxlib", DEFAULT_JAXLIB_VERSION)

        # Strip the leading "v" character from version if present
        if self._version.startswith("v"):
            self._version = self._version[1:]

        if self._from_pip:
            self._install_from_package()
        else:
            self._install_from_source()

        self._instructions()

    def _instructions(self):
        self += comment(
            f"Alpa @ {self._version} (from_pip={self._from_pip}, build_tf={self._build_tf})"
        )
        self += shell(commands=self._commands, chdir=False)

    def _install_from_source(self):
        cmds = []

        editable = ""
        if self._editable:
            editable = "-e"

        jaxlib_version = self._jaxlib

        kwargs = {
            "repository": ALPA_REPO_URL,
            "recursive": self._build_tf,
            "path": self._prefix,
        }

        if self._version == "latest":
            kwargs["branch"] = "main"
        else:
            kwargs["commit"] = f"v{self._version}"

        cmds.append(self.clone_step(**kwargs))
        cmds += [f"cd {self._prefix}/alpa", f"pip install {editable} .[dev]"]

        build_cmds = [
            f"cd {self._prefix}/alpa/build_jaxlib",
            COMPILE_TF_COMMAND,
            f"pip install {editable} ./dist",
        ]

        if self._build_tf:
            cmds += build_cmds
        else:
            cmds += [
                f"pip install {editable} jaxlib=={jaxlib_version} -f https://alpa.ai/wheels.html"
            ]

        self._commands = cmds  # type: ignore

    def _install_from_package(self):
        alpa_version = f"=={self._version}"
        if self._version == "latest":
            alpa_version = ""

        editable = ""
        if self._editable:
            editable = "-e"

        jaxlib_version = self._jaxlib

        self._commands = [
            f"pip install {editable} alpa{alpa_version} jaxlib=={jaxlib_version} -f https://alpa.ai/wheels.html"
        ]
