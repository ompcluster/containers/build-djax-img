# Copyright 2023 DJAX Team
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Ubuntu `update-alternatives` building block."""

from hpccm.building_blocks.base import bb_base
from hpccm.primitives.comment import comment
from hpccm.primitives.shell import shell


class alternative(bb_base):
    """Ubuntu `update-alternatives` building block."""

    def __init__(self, **kwargs):
        super(alternative, self).__init__(**kwargs)

        self.__link = kwargs.get("link")
        self.__name = kwargs.get("name")
        self.__path = kwargs.get("path")
        self.__prio = kwargs.get("priority", 10)

        assert self.__link is not None
        assert self.__name is not None
        assert self.__path is not None

        self.__instructions()

    def __instructions(self):
        command = f"update-alternatives --install {self.__link} {self.__name} {self.__path} {self.__prio}"
        self += comment(f"Update alternative for '{self.__name}'")
        self += shell(commands=[command])
