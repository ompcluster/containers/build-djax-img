# Copyright 2023 DJAX Team
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Neovim building block."""

import posixpath

import hpccm.templates.rm
import hpccm.templates.tar
import hpccm.templates.wget

from hpccm.building_blocks.base import bb_base
from hpccm.primitives.comment import comment
from hpccm.primitives.shell import shell
from hpccm.primitives.environment import environment


class neovim(bb_base, hpccm.templates.rm, hpccm.templates.tar, hpccm.templates.wget):
    """Neovim building block."""

    def __init__(self, **kwargs):
        super(neovim, self).__init__(**kwargs)

        self.__version = kwargs.get("version", "stable")
        self.__build = kwargs.get("build", False)
        self.__prefix = kwargs.get("prefix", "/opt")

        if self.__build:
            raise ValueError(
                "Neovim building block does not support building from source."
            )
        else:
            self.__binary()

        self.__instructions()

    def __instructions(self):
        self += comment(f"Neovim @ {self.__version}")
        self += shell(commands=self.__commands)
        self += environment(
            variables={
                "PATH": "/opt/nvim/bin:$PATH",
                "EDITOR": "nvim",
            }
        )

    def __binary(self):
        tarball = "nvim-linux64.tar.gz"
        version = self.__version
        cmds = []
        url = f"https://github.com/neovim/neovim/releases/download/{version}/{tarball}"

        cmds.append(self.download_step(url=url, directory=self.__prefix))
        cmds.append(
            self.untar_step(
                tarball=posixpath.join(self.__prefix, tarball), directory=self.__prefix
            )
        )
        cmds.append(
            "mv {} {}".format(
                posixpath.join(self.__prefix, tarball.rstrip(".tar.gz")),
                posixpath.join(self.__prefix, "nvim"),
            )
        )
        cmds.append(
            "cd {} && ln -s nvim vim".format(
                posixpath.join(self.__prefix, "nvim", "bin")
            )
        )
        cmds.append(self.cleanup_step(items=[posixpath.join(self.__prefix, tarball)]))

        self.__commands = cmds  # type: ignore
