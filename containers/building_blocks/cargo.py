# Copyright 2023 DJAX Team
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Rust Cargo building block."""

from hpccm.building_blocks.base import bb_base
from hpccm.building_blocks.packages import packages
from hpccm.primitives.comment import comment
from hpccm.primitives.shell import shell
from hpccm.primitives.environment import environment


class cargo(bb_base):
    """Rust Cargo building block."""

    def __init__(self, **kwargs):
        """Initialize building block"""

        super(cargo, self).__init__(**kwargs)

        self.__crates = kwargs.get("crates", [])
        self.__instructions()

    def __instructions(self):
        self += comment("Rust Cargo")
        self += packages(ospackages=["cargo"])
        self += environment(
            variables={"CARGO_HOME": "/opt/cargo", "PATH": "/opt/cargo/bin:$PATH"}
        )

        if self.__crates:
            self += shell(commands=["cargo install {}".format(" ".join(self.__crates))])
