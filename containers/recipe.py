# Copyright 2023 DJAX Team
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import logging
import typing as t

from hpccm import Stage  # type: ignore
from hpccm.primitives import baseimage, environment, shell  # type: ignore
from hpccm.building_blocks import (  # type: ignore
    gnu,
    packages,
    nsight_systems,
    python,
    pip,
)

from containers.primitives import mkdirs, symlink
from containers.building_blocks import cargo, alternative, neovim, alpa, deepwave

# LOGGING ----------------------------------------------------------------------

log = logging.getLogger()

# TYPES ------------------------------------------------------------------------

StageCreationFn = t.Callable[..., Stage]
StagesDict = t.Dict[str, StageCreationFn]

# UTILS ------------------------------------------------------------------------


class StageCreator:
    """Wrapper object around a function that creates an HPCCM Stage."""

    # Default base image, if none is provided.
    ROOT_IMAGE = ("docker", "nvidia/cuda:11.8.0-cudnn8-devel-ubuntu20.04")

    # Class attribute that holds all registered stages
    stages: StagesDict = {}

    @classmethod
    def create(cls, stage_name: str, *args, **kwargs) -> Stage:
        return cls.stages[stage_name](*args, **kwargs)

    @classmethod
    def add(cls, name: str, fn: StageCreationFn, **kwargs) -> StageCreationFn:
        cls.stages[name] = StageCreator(name, fn, **kwargs)
        return fn

    @classmethod
    def stages_names(cls) -> t.List[str]:
        return list(cls.stages.keys())

    def __init__(
        self,
        name: str,
        fn: StageCreationFn,
        base: t.Optional[str] = None,
        features: t.Dict[str, str] = {},
    ):
        self.name = name
        self.fn = fn
        self.docstring = fn.__doc__
        self.features = set(features.keys())
        self.features_dict = features
        self.base = base

    def _create_initial_stage(self, base: t.Optional[t.Tuple[str, str]] = None):
        stage = Stage()

        if base is None:
            if self.base is None:
                base = self.ROOT_IMAGE
            else:
                base = ("recipe", self.base)

        protocol, base_name = base

        if protocol == "docker":
            stage += baseimage(image=base_name)
            return stage

        if protocol == "file":
            stage += baseimage(image=base_name, _bootstrap="localimage")
            return stage

        stage = StageCreator.create(base_name)
        return stage

    def __call__(
        self,
        base: t.Optional[t.Tuple[str, str]] = None,
        features: t.Set[str] = set(),
        format: str = "singularity",
        **kwargs,
    ):
        """Invoke the wrapped stage creation function."""

        if len(diff := (features - self.features)) > 0:
            log.warning(f"Some features will be ignored: {diff}")

        stage = self._create_initial_stage(base)
        return self.fn(stage, features=features, format=format, **kwargs)


def register_stage(
    name: str, base: t.Optional[str] = None, features: t.Dict[str, str] = {}
) -> t.Callable[..., t.Any]:
    """Decorator to register container stages."""

    def decorator(fn: StageCreationFn) -> StageCreationFn:
        return StageCreator.add(name, fn, features=features, base=base)

    return decorator


# CONSTANTS --------------------------------------------------------------------

APT_PACKAGES = [
    "cmake",
    "curl",
    "git",
    "htop",
    "less",
    "tmux",
    "wget",
    "zsh",
]

PIP_PACKAGES = [
    "click",
    "matplotlib",
    "numpy",
    "nvtx",
    "pandas",
    "rich",
    "seaborn",
    "tqdm",
    "black",
    "pytest",
]

CARGO_CRATES = [
    "bat",
    "exa",
    "ripgrep",
]

ENVIRONMENT = {
    "CC": "gcc",
    "CXX": "g++",
    "CUDA_TOOLKIT_PATH": "/usr/local/cuda/",
    "CUDNN_INSTALL_PATH": "/usr",
    "LIBRARY_PATH": "/usr/lib/x86_64-linux-gnu:/usr/local/cuda/lib64:/usr/local/cuda/lib64/stubs:$LIBRARY_PATH",
    "LD_LIBRARY_PATH": "/usr/lib/x86_64-linux-gnu:/usr/local/cuda/lib64:/usr/local/cuda/lib64/stubs:$LD_LIBRARY_PATH",
}

# STAGE CREATION ---------------------------------------------------------------


@register_stage(
    "djax-base",
    features={
        "neovim": "Include neovim text editor.",
        "cargo": "Include Rust Cargo packages.",
        "nsys": "Include NVIDIA Nsight Systems.",
    },
)
def create_stage_base(
    stage: Stage, *, features: t.Set[str] = set(), format: str = "singularity", **kwargs
) -> Stage:
    """Create recipe for compiling Alpa/Tensorflow."""

    stage += gnu(version="7")
    stage += environment(variables=ENVIRONMENT)
    stage += mkdirs(dirs=["/opt/alpa", "/opt/deepwave"])

    # Symlink cuda library otherwise cupy can't find it.
    # TODO: Create a new building block for cupy.
    stage += symlink(
        target="libcuda.so", link="libcuda.so.1", workdir="/usr/local/cuda/lib64/stubs"
    )

    # Install Apt packages
    stage += packages(apt=APT_PACKAGES)

    # Install Neovim
    if "neovim" in features:
        stage += neovim(version="stable")

    # Install Cargo crates
    if "cargo" in features:
        stage += cargo(crates=CARGO_CRATES)

    # Install NVIDIA Nsight Systems
    if "nsys" in features:
        stage += nsight_systems(version="2022.5.1")

    # Install Python
    stage += python(python2=False, python3=True, devel=True)

    # Install Pip and Pip packages
    stage += pip(pip="pip3", upgrade=True, packages=PIP_PACKAGES + ["cupy-cuda11x"])

    # Set Python 3 as default
    stage += alternative(
        link="/usr/local/bin/python",
        name="python",
        path="/usr/bin/python3",
        priority=20,
    )

    return stage


@register_stage("djax-alpa-runtime", base="djax-base")
def create_stage_alpa_runtime(
    stage: Stage, *, features: t.Set[str] = set(), format: str = "singularity", **kwargs
) -> Stage:
    """Create recipe for Alpa/Tensorflow runtime."""
    stage += alpa(from_pip=True)
    return stage


@register_stage(
    "djax-alpa-dev",
    base="djax-base",
    features={
        "tensorflow": "Build tensorflow-alpa from source.",
    },
)
def create_stage_alpa_dev(
    stage: Stage, *, features: t.Set[str] = set(), format: str = "singularity", **kwargs
) -> Stage:
    """Create recipe for Alpa/Tensorflow development."""
    build_tensorflow = "tensorflow" in features
    stage += alpa(from_pip=False, editable=True, build_tensorflow=build_tensorflow)
    return stage


@register_stage("djax-deepwave-runtime", base="djax-alpa-runtime")
def create_stage_deepwave_runtime(
    stage: Stage, *, features: t.Set[str] = set(), format: str = "singularity", **kwargs
) -> Stage:
    """Create recipe for Deepwave runtime."""
    stage += deepwave(editable=False, extras=[])
    return stage


@register_stage(
    "djax-deepwave-dev",
    base="djax-alpa-dev",
    features={
        "tui": "Enable Deepwave Terminal UI.",
    },
)
def create_stage_deepwave_dev(
    stage: Stage, *, features: t.Set[str] = set(), format: str = "singularity", **kwargs
) -> Stage:
    """Create recipe for Deepwave development."""
    extras = ["dev"]
    if "tui" in features:
        extras += ["tui"]
    stage += deepwave(editable=True, extras=extras)
    return stage
